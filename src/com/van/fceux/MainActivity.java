package com.van.fceux;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.van.fceux.R;
import com.van.fceux.GL2JNIView;
import com.van.fceux.MainActivity;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.facebook.*;
import com.facebook.Session.StatusCallback;
import com.facebook.model.*;

public class MainActivity extends Activity {

    static 
    {
        System.loadLibrary("nes");
    }
    
    GL2JNIView mView;
	public static MainActivity sInstance = null;
	public static Handler sHander = new Handler();
	public static String sNesFile = "";
	public static String saveStateSlot = "";
	public static String autosaveFile = "";
	public static boolean resetGame = false;
	public static boolean loadAutoSave = false;
	public static String admodId = "a152f241d7cfb80";
	public static UiLifecycleHelper uiHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sInstance = this;
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		Intent oIntent = this.getIntent();
		sNesFile = oIntent.getStringExtra("file");
		autosaveFile = oIntent.getStringExtra("autosave");
		resetGame = oIntent.getBooleanExtra("resetGame", false);
		
		this.setContentView(R.layout.main);
		
		mView = new GL2JNIView(getApplication());
		ViewGroup gameContainer = (ViewGroup)this.findViewById(R.id.gameContainer);
		gameContainer.addView(mView);
		
		
		AdView adView = new AdView(this, AdSize.BANNER, "a152f241d7cfb80");
		ViewGroup adsContainer = (ViewGroup)this.findViewById(R.id.adsContainer);
		adsContainer.addView(adView);
		AdRequest oAdRequest = new AdRequest();
		adView.loadAd(oAdRequest);
		
		
		uiHelper = new UiLifecycleHelper(MainActivity.sInstance, new StatusCallback(){
			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				
			}});
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == 1 || requestCode == 2)
		{
			mView.mRenderer.mGL2JNILib.setPause(false, saveStateSlot);
		}
		else
		{
			Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
		}
	}
	
	@Override 
	public void onSaveInstanceState(Bundle outState)
	{
		//Toast.makeText(this, "need save:" + autosaveFile, 0).show();
		mView.mRenderer.mGL2JNILib.saveState(autosaveFile);
	}

	@Override
	public void onBackPressed()
	{
		//Toast.makeText(this, "need save:" + autosaveFile , 0).show();
		mView.mRenderer.mGL2JNILib.saveState(autosaveFile);
		finish();
	}
}
