/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.van.fceux;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.List;
import java.util.zip.CRC32;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.ByteArrayBuffer;

import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.Facebook;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

// Wrapper for native library

public class GL2JNILib {

     static 
     {
         System.loadLibrary("nes");
     }

    /**
     * @param width the current view width
     * @param height the current view height
     */
     public native void init(AssetManager assetManager, int width, int height, String strNesFile, String strAutoSaveFile, boolean resetGame, boolean loadAutoSave);
     public native void step();
     public native void onTouchDown(int touchid, int x, int y);
     public native void onTouchMove(int touchid, int x, int y);
     public native void onTouchUp(int touchid, int x, int y);
     public native void saveState(String strFile);
     public native void setPause(boolean pause, String strState);
     
     public static int getImageWidthInAsset(String strFileName)
     {
		try 
		{
			InputStream is;
			AssetManager assetManager = MainActivity.sInstance.getAssets();
			is = assetManager.open(strFileName);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, options);
			is.close();
			return options.outWidth;
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return -1;
		}
     }
     
     public static int getImageHeightInAsset(String strFileName)
     {
    	try
 		{
 			InputStream is;
 			AssetManager assetManager = MainActivity.sInstance.getAssets();
 			is = assetManager.open(strFileName);
 			BitmapFactory.Options options = new BitmapFactory.Options();
 			options.inJustDecodeBounds = true;
 			BitmapFactory.decodeStream(is, null, options);
 			is.close();
 			return options.outHeight;
 		} 
 		catch (IOException e) 
 		{
 			e.printStackTrace();
 			return -1;
 		}
     }
     
     public static int[] getImageFromAsset(String strFileName)
     {
    	try
  		{
  			InputStream is;
  			AssetManager assetManager = MainActivity.sInstance.getAssets();
  			is = assetManager.open(strFileName);
  			Bitmap bm = BitmapFactory.decodeStream(is);
  			is.close();
  			int[] ret = new int[bm.getWidth() * bm.getHeight()];
  			bm.getPixels(ret, 0, bm.getWidth(), 0, 0, bm.getWidth(), bm.getHeight());
  			return ret;
  		} 
  		catch (IOException e) 
  		{
  			e.printStackTrace();
  			return null;
  		}
     }
     
     public static long crc32(int[] array)
     {
         CRC32 c = new CRC32();
         for (int i : array)
         {
             c.update(i);
         }
         return c.getValue();
     }
     
 	public static String getExternalStorageDirectory()
 	{
 		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/" + MainActivity.sInstance.getPackageName();
 	}
 	
 	public static void startSaveStateActivity(String strPrefix)
 	{
 		Intent oIntent = new Intent(MainActivity.sInstance, SaveStateActivity.class);
 		oIntent.putExtra("prefix", strPrefix);
 		MainActivity.sInstance.startActivityForResult(oIntent, 1);
 	}
 	
 	public static void startLoadStateActivity(String strPrefix)
 	{
 		Intent oIntent = new Intent(MainActivity.sInstance, LoadStateActivity.class);
 		oIntent.putExtra("prefix", strPrefix);
 		MainActivity.sInstance.startActivityForResult(oIntent, 2);
 	}
 	
    private static boolean hasPublishPermission(Session session) {
        return session != null && session.getPermissions().contains("publish_actions");
    }
 	
 	public static void shareClick()
 	{
 		try
 		{
 			Toast.makeText(MainActivity.sInstance, "Upload screenshot to Facebook... Please wait", Toast.LENGTH_SHORT).show();
 			
//	 		Intent oIntent = new Intent(Intent.ACTION_SEND);
//	 		//oIntent.setType("text/plain");
//	 		oIntent.setType("image/png");
//	 		//oIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.van.fceux");
//	 		Uri uri = Uri.parse(getExternalStorageDirectory() + "/snapshot.png");
//	 		oIntent.putExtra(Intent.EXTRA_STREAM, uri);
//	 		PackageManager pm = MainActivity.sInstance.getPackageManager();
//	 		List<ResolveInfo> activityList = pm.queryIntentActivities(oIntent, 0);
//	 		for(ResolveInfo app : activityList)
//	 		{
//	 			if(app.activityInfo.name.contains("facebook.katana"))
//	 			{
//	 				ActivityInfo activity = app.activityInfo;
//	 				ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
//	 				oIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//	 				oIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//	 				oIntent.setComponent(name);
//	 				MainActivity.sInstance.startActivity(oIntent);
//	 				//break;
//	 			}
//	 		}
//	 		
//	 		MainActivity.sInstance.startActivity(Intent.createChooser(oIntent, "Share with"));
 		
			Session session = Session.openActiveSession(MainActivity.sInstance, true, new Session.StatusCallback() {
				@Override
				public void call(Session session, SessionState arg1, Exception arg2) {
					try
					{
						if(session.isOpened())
						{
				            if (hasPublishPermission(session))
				            {
								Bitmap bm = BitmapFactory.decodeFile(getExternalStorageDirectory() + "/snapshot.png");
								Request.Callback callback = new Request.Callback() {
									@Override
									public void onCompleted(Response response) {
										try
										{
											FacebookRequestError error = response.getError();
											if(error != null)
											{
												Toast.makeText(MainActivity.sInstance, "Upload screenshot error: " + error.getErrorMessage(), Toast.LENGTH_LONG).show();
											}
											else
											{
												Toast.makeText(MainActivity.sInstance, "Upload screenshot successfull", Toast.LENGTH_LONG).show();
												String strId = response.getGraphObject().getProperty("id").toString();
												String strUrl = "http://m.facebook.com/photo.php?fbid" + strId;
												//HtmlDownloader downloader = new HtmlDownloader();
												//ExecuteAsyncTask(downloader, strUrl);
												
												try
												{
													if (FacebookDialog.canPresentShareDialog(MainActivity.sInstance.getApplicationContext(), 
					                                         FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) 
													{
														FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(MainActivity.sInstance).setLink("https://play.google.com/store/apps/details?id=com.van.fceux").build();
														MainActivity.uiHelper.trackPendingDialogCall(shareDialog.present());
													}
													else
													{
														Bundle params = new Bundle();
													    params.putString("link", "https://play.google.com/store/apps/details?id=com.van.fceux");

													    WebDialog feedDialog = (
													        new WebDialog.FeedDialogBuilder(MainActivity.sInstance,
													            Session.getActiveSession(),
													            params))
													        .setOnCompleteListener(new OnCompleteListener() {

													            @Override
													            public void onComplete(Bundle values, FacebookException error)
													            {
													                
													            }
													        })
													        .build();
													    feedDialog.show();
													}
												}
												catch (Exception ex)
												{
													
												}
											}
										}
										catch(Exception ex)
										{
											
										}
									}
								};
								Request request = Request.newUploadPhotoRequest(session, bm, callback);
								Bundle params = request.getParameters();
								params.putString("message", "I'm playing this game.\n https://play.google.com/store/apps/details?id=com.van.fceux ");
								request.executeAsync();
								
				                return;
				            }
				            else// if (session.isOpened()) 
				            {
				                session.requestNewPublishPermissions(new Session.NewPermissionsRequest(MainActivity.sInstance, "publish_actions"));
				                return;
				            }					
							
							//FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(MainActivity.sInstance).setLink("https://play.google.com/store/apps/details?id=com.van.fceux").build();
							//MainActivity.uiHelper.trackPendingDialogCall(shareDialog.present());
							
						}
					}
					catch(Exception ex)
					{
						
					}
				}
			});
 		}
 		catch(Exception ex)
 		{
 			
 		}
 	}
 	
 	
	@SuppressLint("NewApi")
	public static void ExecuteAsyncTask(AsyncTask<String, ?, ?> oTask, String... params)
	{
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
		{
			oTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, params);
		}
		else
		{
			oTask.execute(params);
		}
	}

}




class DownloadResult {
	public String url;
	public String content = "";
	public boolean NetworkError = false;
	public DownloadResult(String strUrl)
	{
		url = strUrl;
	}
}

class HtmlDownloader extends AsyncTask<String, Void, DownloadResult> {
	
	@Override
	protected DownloadResult doInBackground(String... args) {
		DownloadResult oRet = new DownloadResult(args[0]);
		ByteArrayBuffer oBAF = new ByteArrayBuffer(64);
		try
		{
			HttpClient oHttpClient = new DefaultHttpClient();
			HttpContext oLocalContext = new BasicHttpContext();
			HttpGet oHttpGet = new HttpGet(args[0]);
			HttpResponse oResponse = oHttpClient.execute(oHttpGet, oLocalContext);
			InputStream oIS = oResponse.getEntity().getContent();
			BufferedInputStream oBIS = new BufferedInputStream(oIS);
			byte[] temp = new byte[64];
			
			while(true)
			{
				int n = oBIS.read(temp);
				if(n == -1)
					break;
				if(n > 0)
					oBAF.append(temp, 0, n);
			}
		}
		catch(OutOfMemoryError e)
		{
			return oRet;
		}
		catch(EOFException e)
		{
			oRet.content = new String(oBAF.toByteArray());
			return oRet;
		}
		catch(UnknownHostException e)
		{
			oRet.NetworkError = true;
			return oRet;
		}
		catch(Exception e)
		{
			return oRet;
		}
		oRet.content = new String(oBAF.toByteArray());
		return oRet;
	}

	@Override
	protected void onPostExecute(DownloadResult obj)
	{
		String strTemp = obj.content;
		while(true)
		{
			int n = strTemp.indexOf("<a ");
			if(n >= 0)
			{
				strTemp = strTemp.substring(n);
				n = strTemp.indexOf("href=");
				strTemp = strTemp.substring(n + 6);
				n = strTemp.indexOf('"');
				String strLink = strTemp.substring(0, n);
				if(strLink.endsWith(".jpg") || strLink.endsWith(".png"))
				{
					Toast.makeText(MainActivity.sInstance, strLink, Toast.LENGTH_LONG).show();
				}
			}
			else
			{
				break;
			}
		}
	}
}