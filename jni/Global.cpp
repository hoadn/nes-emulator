#include "Global.h"

#ifdef ANDROID
JNIEnv* Global::env = 0;
JavaVM* Global::javaVM = 0;
jobject Global::jObj = 0;
#endif
int Global::width = 0;
int Global::height = 0;
float Global::density = 1.0f;

#ifdef ANDROID
void Global::init(JNIEnv* env, jobject jObj, int width, int height)
{
	Global::env = env;
	Global::jObj = jObj;
	env->GetJavaVM(&Global::javaVM);
	Global::width = width;
	Global::height = height;
}
#else
void Global::init(int width, int height)
{
	Global::width = width;
	Global::height = height;
}
#endif