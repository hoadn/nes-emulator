#pragma once

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <string>
#include <sstream>
#include <vector>
#include <time.h>
#include "Exception.h"
#include "tinyxml.h"

#define GETBIT(NUM, B) (NUM & Utilities::BIT##B)

class Utilities
{
public:
	static const int BIT0 = 1;
	static const int BIT1 = 2;
	static const int BIT2 = 4;
	static const int BIT3 = 8;
	static const int BIT4 = 16;
	static const int BIT5 = 32;
	static const int BIT6 = 64;
	static const int BIT7 = 128;
	static const int BIT8 = 256;


	static void checkGLError(const std::string& strFile, int nLine);
	static int getPowerOf2Larger(int n);
	static int getPowerOf2Nearest(int n);
	static bool getbit(int num, int bitnum);
	static int setbit(int& num, int bitnum, bool state);
	static int reverseByte(int nibble);
#ifdef ANDROID
	static double now_ms();
	static long long crc32(const std::vector<int>& data);
#else
	static double now_ms();
#endif
	static std::string hex(long long l);
	static int squareLength(int x1, int y1, int x2, int y2)
	{
		int dx = x2 - x1;
		int dy = y2 - y1;
		return dx * dx + dy * dy;
	}

	static std::string getExternalStorageDirectory();
	static void setXmlAttribute(TiXmlElement& e, const char* name, const char* value);
	static void setXmlAttribute(TiXmlElement& e, const char* name, int value);
	static void setXmlAttribute(TiXmlElement& e, const char* name, bool value);
	static void setXmlAttribute(TiXmlElement& e, const char* name, int* values, int size);
	static void setXmlAttribute(TiXmlElement& e, const char* name, bool* values, int size);
	static void setXmlAttribute(TiXmlElement& e, const char* name, std::vector<bool>& values);

	static void readXmlAttribute(TiXmlElement* e, const char* name, int& outvalue);
	static void readXmlAttribute(TiXmlElement* e, const char* name, bool& outvalue);
	static void readXmlAttribute(TiXmlElement* e, const char* name, int* outvalues, int size);
	static void readXmlAttribute(TiXmlElement* e, const char* name, bool* outvalues, int size);
	static void readXmlAttribute(TiXmlElement* e, const char* name, std::vector<bool>& values);
};

//#ifdef DEBUG
//#define CHECKGLERROR Utilities::checkGLError(__FILE__, __LINE__)
//#else
#define CHECKGLERROR
//#endif
