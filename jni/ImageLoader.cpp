#include "ImageLoader.h"
#include "Global.h"
#include "Log.h"
#include <memory>
#ifdef ANDROID
#include <jni.h>

int VImageLoader::getImageWidth(const std::string& strFileName)
{
	jint res = Global::javaVM->AttachCurrentThread(&Global::env, NULL);
	if(res == JNI_ERR)
	{
		throw VException("JNI can not attach to current thread");
	}

	jclass clazz = Global::env->FindClass("com/van/fceux/GL2JNILib");
	jmethodID methodId = Global::env->GetStaticMethodID(clazz, "getImageWidthInAsset", "(Ljava/lang/String;)I");

	jstring jstr = Global::env->NewStringUTF(strFileName.c_str());
	int width = Global::env->CallStaticIntMethod(clazz, methodId, jstr);// CallIntMethod(s_pObj, methodId, jstr);
	Global::env->DeleteLocalRef(jstr);
	//s_pJavaVM->DetachCurrentThread();

	if(width < 0)
	{
		std::stringstream ss;
		ss << "Can not get width of " << strFileName;
		throw VException(ss.str());
	}

	return width;
}

int VImageLoader::getImageHeight(const std::string& strFileName)
{
	jint res = Global::javaVM->AttachCurrentThread(&Global::env, NULL);
	if(res == JNI_ERR)
	{
		throw VException("JNI can not attach to current thread");
	}

	jclass clazz = Global::env->FindClass("com/van/fceux/GL2JNILib");
	jmethodID methodId = Global::env->GetStaticMethodID(clazz, "getImageHeightInAsset", "(Ljava/lang/String;)I");

	jstring jstr = Global::env->NewStringUTF(strFileName.c_str());
	int height = Global::env->CallStaticIntMethod(clazz, methodId, jstr); //CallIntMethod(s_pObj, methodId, jstr);
	Global::env->DeleteLocalRef(jstr);
	//s_pJavaVM->DetachCurrentThread();

	if(height < 0)
	{
		std::stringstream ss;
		ss << "Can not get height of " << strFileName;
		throw VException(ss.str());
	}

	return height;
}

std::shared_ptr<int> VImageLoader::getImage(const std::string& strFileName)
{
	jint res = Global::javaVM->AttachCurrentThread(&Global::env, NULL);
	if(res == JNI_ERR)
	{
		throw VException("JNI can not attach to current thread");
	}

	jclass clazz = Global::env->FindClass("com/van/fceux/GL2JNILib");
	jmethodID methodId = Global::env->GetStaticMethodID(clazz, "getImageFromAsset", "(Ljava/lang/String;)[I");
	jstring jstr = Global::env->NewStringUTF(strFileName.c_str());
	jintArray jdata = (jintArray)Global::env->CallStaticObjectMethod(clazz, methodId, jstr); //CallObjectMethod(s_pObj, methodId, jstr);
	Global::env->DeleteLocalRef(jstr);
	if(jdata)
	{
		int size = Global::env->GetArrayLength(jdata);
		jint* pjdata = Global::env->GetIntArrayElements(jdata, 0);
		std::shared_ptr<int> retData(new int[size], std::default_delete<int[]>());
		for(int i=0; i<size; i++)
		{
			retData.get()[i] = pjdata[i];
		}
		//s_pJavaVM->DetachCurrentThread();
		return retData;
	}
	else
	{
		//s_pJavaVM->DetachCurrentThread();
		//return NULL;
		std::stringstream ss;
		ss << "Can not load image file:" << strFileName;
		throw VException(ss.str());
	}
}
#endif

#ifdef _WIN32

int VImageLoader::getImageWidth(const std::string& strFileName)
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(strFileName.c_str(), 0);//Automatically detects the format(from over 20 formats!)
	FIBITMAP* image = FreeImage_Load(format, strFileName.c_str());
	int w = FreeImage_GetWidth(image);
	FreeImage_Unload(image);
	return w;
}

int VImageLoader::getImageHeight(const std::string& strFileName)
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(strFileName.c_str(), 0);//Automatically detects the format(from over 20 formats!)
	FIBITMAP* image = FreeImage_Load(format, strFileName.c_str());
	int h = FreeImage_GetHeight(image);
	FreeImage_Unload(image);
	return h;
}

std::shared_ptr<int> VImageLoader::getImage(const std::string& strFileName)
{
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(strFileName.c_str(), 0);//Automatically detects the format(from over 20 formats!)
	FIBITMAP* image = FreeImage_Load(format, strFileName.c_str());

	FIBITMAP* temp = image;
	image = FreeImage_ConvertTo32Bits(image);
	FreeImage_Unload(temp);

	int w = FreeImage_GetWidth(image);
	int h = FreeImage_GetHeight(image);

	std::shared_ptr<int> ret(new int[w * h], std::default_delete<int[]>());
	int* pixels = (int*)FreeImage_GetBits(image);
	for(int i=0; i<h; i++)
	{
		memcpy(&(ret.get()[i * w]), &pixels[(h-i-1) * w], w * sizeof(int));
	}
	//memcpy(ret.get(), pixels, w * h * sizeof(int));
	FreeImage_Unload(image);
	return ret;
}

#endif
