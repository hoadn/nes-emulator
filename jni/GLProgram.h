#pragma once

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "AssetManager.h"
#include "Utilities.h"

class VGLProgram
{
public:
	VGLProgram();
	~VGLProgram();

	void loadFromString(const std::string& strVs, const std::string& strFs);
#ifdef ANDROID
	void loadFromAsset(const std::string& strVsFileName, const std::string& strFsFileName);
#endif

	void unload();

	int getAttributeId(const std::string& strAttrName);
	int getUniformId(const std::string& strUniformName);

	void use();
	void bindVector(const std::string& strAttrName, int size, const void* data, GLsizei stride = 0);
	void bindVector2(const std::string& strAttrName, const void* data, GLsizei stride = 0);
	void bindVector3(const std::string& strAttrName, const void* data, GLsizei stride = 0);
	void bindVector4(const std::string& strAttrName, const void* data, GLsizei stride = 0);

	bool isLoaded();
private:
	GLuint loadShader(int nType, const std::string& strShader);

private:
	GLuint mVsId;
	GLuint mFsId;
	GLuint mProgramId;
};
