#include "Utilities.h"
#include "ImageLoader.h"
#include "Log.h"
#include "Global.h"
#include "time.h"


void Utilities::checkGLError(const std::string& strFile, int nLine)
{
	bool bHasError = false;
	std::stringstream ss;
	ss << "checkGLError failed at " << strFile << ":" << nLine << std::endl;
    for (GLint error = glGetError(); error; error = glGetError())
    {
    	ss << "  error code:" << error << std::endl;
        bHasError = true;
    }

    if(bHasError)
    {
    	LOGE(ss.str().c_str());
    	throw VException(ss.str());
    }
}

int Utilities::getPowerOf2Larger(int n)
{
	int temp = 1;
	while(temp <= n)
	{
		temp = temp * 2;
	}
	return temp;
}

int Utilities::getPowerOf2Nearest(int n)
{
	int temp = 1;
	int prev = 1;
	while(temp <= n)
	{
		prev = temp;
		temp = temp * 2;
	}
	int d1 = n - prev;
	int d2 = temp - n;
	if(d1 < d2)
		return prev;
	else
		return temp;
}

#ifdef ANDROID
double Utilities::now_ms()
{
	struct timespec res;
	clock_gettime(CLOCK_REALTIME, &res);
	return 1000.0 * res.tv_sec + (double) res.tv_nsec / 1e6;
}
#else
double Utilities::now_ms()
{
	return 0;
}
#endif

bool Utilities::getbit(int num, int bitnum)
{
        //returns the nth bit of the int provided
        //bit numbers are zero indexed
        return ((num & (1 << bitnum)) != 0);
}

int Utilities::setbit(int& num, int bitnum, bool state)
{
	return (state) ? (num | (1 << bitnum)) : (num & ~(1 << bitnum));
}

int Utilities::reverseByte(int nibble)
{
	nibble &= 0xff;

	int ret = 0;
	ret |= ((nibble & 1) << 7);
	ret |= ((nibble & 2) << 5);
	ret |= ((nibble & 4) << 3);
	ret |= ((nibble & 8) << 1);
	ret |= ((nibble & 16) >> 1);
	ret |= ((nibble & 32) >> 3);
	ret |= ((nibble & 64) >> 5);
	ret |= ((nibble & 128) >> 7);

	return ret;// & 0xff;

    //reverses 8 bits packed into int.
    //return (Integer.reverse(nibble) >> 24) & 0xff;
}

#ifdef ANDROID
long long Utilities::crc32(const std::vector<int>& data)
{
	JNIEnv* env = Global::env;
	JavaVM* javaVM = Global::javaVM;

	jint res = javaVM->AttachCurrentThread(&env, NULL);
	if(res == JNI_ERR)
	{
		throw VException("JNI can not attach to current thread");
	}

	jclass clazz = env->FindClass("com/van/fecux/GL2JNILib");
	jmethodID methodId = env->GetStaticMethodID(clazz, "crc32", "([I)J");
	jintArray jdata = env->NewIntArray(data.size());
	env->SetIntArrayRegion(jdata, 0, data.size(), data.data());
	jlong result = env->CallStaticLongMethod(clazz, methodId, jdata); //CallLongMethod(s_pObj, methodId, jdata);
	env->DeleteLocalRef(jdata);
	return result;
}
#endif

std::string Utilities::hex(long long l)
{
	std::stringstream ss;
	ss << std::hex << l;
	return ss.str();
}

std::string Utilities::getExternalStorageDirectory()
{
#ifdef ANDROID
	JNIEnv* env = Global::env;
	JavaVM* javaVM = Global::javaVM;

	jint res = javaVM->AttachCurrentThread(&env, NULL);
	if(res == JNI_ERR)
	{
		throw VException("JNI can not attach to current thread");
	}

	jclass clazz = env->FindClass("com/van/fceux/GL2JNILib");
	jmethodID methodId = env->GetStaticMethodID(clazz, "getExternalStorageDirectory", "()Ljava/lang/String;");

	jstring result = (jstring)env->CallStaticObjectMethod(clazz, methodId); //CallLongMethod(s_pObj, methodId, jdata);
	std::string ret = env->GetStringUTFChars(result, NULL);
	return ret;
#else
	return "D:\\";
#endif
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, const char* value)
{
	e.SetAttribute(name, value);
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, int value)
{
	e.SetAttribute(name, value);
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, bool value)
{
	e.SetAttribute(name, value ? "true" : "false");
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, int* values, int size)
{
	std::stringstream ss;
	int n = size - 1;
	for(int i=0; i<n; ++i)
	{
		ss << values[i] << " ";
	}
	ss << values[size - 1];
	e.SetAttribute(name, ss.str().c_str());
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, bool* values, int size)
{
	std::stringstream ss;
	int n = size - 1;
	for(int i=0; i<n; ++i)
	{
		if(values[i])
			ss << "true " << " ";
		else
			ss << "false " << " ";
	}

	if(values[size - 1])
		ss << "true";
	else
		ss << "false";
	e.SetAttribute(name, ss.str().c_str());
}

void Utilities::setXmlAttribute(TiXmlElement& e, const char* name, std::vector<bool>& values)
{
	int n = values.size() - 1;
	std::stringstream ss;
	for(int i=0; i<n; ++i)
		{
			if(values[i])
				ss << "true " << " ";
			else
				ss << "false " << " ";
		}

		if(values[n])
			ss << "true";
		else
			ss << "false";
		e.SetAttribute(name, ss.str().c_str());
}

void Utilities::readXmlAttribute(TiXmlElement* e, const char* name, int& outvalue)
{
	e->Attribute(name, &outvalue);
}

void Utilities::readXmlAttribute(TiXmlElement* e, const char* name, bool& outvalue)
{
	const char* value = e->Attribute(name);
	if(value[0] == 't' || value[0] == 'T') // check first character only
		outvalue = true;
	else
		outvalue = false;
}

void Utilities::readXmlAttribute(TiXmlElement* e, const char* name, int* outvalues, int size)
{
	const char* str = e->Attribute(name);
	std::istringstream in(str);
	for(int i=0; i<size; ++i)
	{
		in >> outvalues[i];
	}
}

void Utilities::readXmlAttribute(TiXmlElement* e, const char* name, bool* outvalues, int size)
{
	const char* str = e->Attribute(name);
	std::istringstream in(str);
	std::string strTemp;
	for(int i=0; i<size; ++i)
	{
		in >> strTemp;
		if(strTemp[0] == 't' || strTemp[0] == 'T')
			outvalues[i] = true;
		else
			outvalues[i] = false;
	}
}

void Utilities::readXmlAttribute(TiXmlElement* e, const char* name, std::vector<bool>& outvalues)
{
	const char* str = e->Attribute(name);
	std::istringstream in(str);
	std::string strTemp;
	for(int i=0; i<outvalues.size(); ++i)
	{
		in >> strTemp;
		if(strTemp[0] == 't' || strTemp[0] == 'T')
			outvalues[i] = true;
		else
			outvalues[i] = false;
	}
}
