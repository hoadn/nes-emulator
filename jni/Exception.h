#pragma once

#include <exception>
#include <string>
#include <sstream>
#include "LOG.h"

class VException : public std::exception
{
public:
	VException(const std::string& strMessage);
	~VException() throw();
	virtual const char* what() const throw();
private:
	std::string m_strMessage;
};
