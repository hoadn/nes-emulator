#pragma once

#ifdef ANDROID
#include <jni.h>
#endif

class Global
{
public:
#ifdef ANDROID
	static void init(JNIEnv* env, jobject jObj, int width, int height);
#else
	static void init(int width, int height);
#endif

#ifdef ANDROID
	static JNIEnv* env;
	static JavaVM* javaVM;
	static jobject jObj;
#endif
	static int width;
	static int height;
	static float density;
};
