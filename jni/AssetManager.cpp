#include "AssetManager.h"
#include "Exception.h"
#ifdef ANDROID
AAssetManager* VAssetManager::s_pAssetManager = NULL;

void VAssetManager::init(AAssetManager* assetManager)
{
	s_pAssetManager = assetManager;
}

std::string VAssetManager::readAsString(const std::string& strFileName)
{
	if(!s_pAssetManager)
		throw VException("VAssetManager has not initialized");

	AAsset* asset = AAssetManager_open(s_pAssetManager, strFileName.c_str(), AASSET_MODE_UNKNOWN);
	if(asset == NULL)
	{
		std::stringstream ss;
		ss << "Can not read asset:" << strFileName;
		throw VException(ss.str());
	}
	else
	{
		off_t length = AAsset_getLength(asset);
		std::shared_ptr<char> buf(new char[length + 1], std::default_delete<char[]>());
		AAsset_read(asset, buf.get(), length);
		buf.get()[length] = 0;
		std::string strRet(buf.get());
		AAsset_close(asset);
		return strRet;
	}
	return "";
}

int VAssetManager::getFileSize(const std::string& strFileName)
{
	if(!s_pAssetManager)
		throw VException("VAssetManager has not initialized");

	AAsset* asset = AAssetManager_open(s_pAssetManager, strFileName.c_str(), AASSET_MODE_UNKNOWN);
	if(asset == NULL)
	{
		std::stringstream ss;
		ss << "Can not read asset:" << strFileName;
		throw VException(ss.str());
	}
	else
	{
		off_t length = AAsset_getLength(asset);
		AAsset_close(asset);
		return length;
	}
}
std::shared_ptr<unsigned char> VAssetManager::readFile(const std::string& strFileName)
{
	if(!s_pAssetManager)
		throw VException("VAssetManager has not initialized");

	AAsset* asset = AAssetManager_open(s_pAssetManager, strFileName.c_str(), AASSET_MODE_UNKNOWN);
	if(asset == NULL)
	{
		std::stringstream ss;
		ss << "Can not read asset:" << strFileName;
		throw VException(ss.str());
	}
	else
	{
		off_t length = AAsset_getLength(asset);
		std::shared_ptr<unsigned char> buf(new unsigned char[length], std::default_delete<unsigned char[]>());
		AAsset_read(asset, buf.get(), length);
		AAsset_close(asset);
		return buf;
	}
}
#endif
