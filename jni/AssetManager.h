#pragma once

#ifdef ANDROID
#include <jni.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <string>
#include <sstream>
#include <memory>
#include "Exception.h"

class VAssetManager
{
public:
	static void init(AAssetManager* assetManager);
	static std::string readAsString(const std::string& strFileName);
	static int getFileSize(const std::string& strFileName);
	static std::shared_ptr<unsigned char> readFile(const std::string& strFileName);

	static AAssetManager* s_pAssetManager;
};
#endif
