#pragma once

#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <memory>
#include "GLProgram.h"
#include <list>

using namespace std;

class VTexture
{
public:
	VTexture();
	~VTexture();

	void init(const std::string& strFileName);
	void init(int width, int height, int* data = NULL);
	void reload();
	void destroy();
	void update(int x, int y, int width, int height, int* pixels);
	int getWidth();
	int getHeight();

	GLuint getTextureId();

	void render(int x, int y, int width, int height);
	bool pointInRenderRegion(int x, int y);

	static void notifyDeviceReset();
public:
	static VGLProgram sProgram;
	static int screenWidth, screenHeight;
	bool enableBlend;
	float alpha;
	int userdata;
	int renderX, renderY, renderWidth, renderHeight;

private:
	int mWidth;
	int mHeight;
	GLuint mTextureId;
	std::string mStrFileName;
	static list<VTexture*> sTextureList;
	shared_ptr<int> mData;
};
