#pragma once

#ifdef ANDROID
#include <jni.h>
#endif
#include <string>
#include <memory>
#include "Exception.h"
#ifdef _WIN32
#include <FreeImage.h>
#endif

class VImageLoader
{
public:
	static int getImageWidth(const std::string& strFileName);
	static int getImageHeight(const std::string& strFileName);
	static std::shared_ptr<int> getImage(const std::string& strFileName);
};
