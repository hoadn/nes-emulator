#include "Exception.h"
#include "Log.h"

VException::VException(const std::string& strMessage)
{
	m_strMessage = strMessage;
	LOGE(m_strMessage.c_str());
}

VException::~VException() throw()
{

}

const char* VException::what() const throw()
{
	return m_strMessage.c_str();
}
